<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Etablissement;
use App\Commande;
use App\User;

class Crud_proController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nb_reservation = 0;
        $reservation = 0;
        $nb_masks = 0;

        $user = auth()->user();
        $societe = Etablissement::get()->where('id_user', $user->id)->first();
        if (!empty($societe->id)) {
            $reservation = Commande::where('id_etablissement', $societe->id)->get();
            $nb_reservation = count($reservation);

            foreach ($reservation as $idk) { 
                $nb_masks = $nb_masks + $idk->nb_masks_take;
            }
        };


        return view('crud_pro', ['societe' => $societe, 'user' => $user, 'nb_reservation' => $nb_reservation, 'reservation' => $reservation, 'nb_masks' => $nb_masks]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = auth()->user();

        Etablissement::create([
            'name' => $request->name,
            'code' => $request->code,
            'adresse' => $request->adresse,
            'ville' => $request->ville,
            'nb_masks' => $request->nb,
            'max_masks' => $request->max,
            'id_user' => $user->id

        ]);
        return redirect()->route('crud_pro.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mon_tres_jolie_commerce = Etablissement::find($id);

        $nouveau_nombre_de_masque = $mon_tres_jolie_commerce->nb_masks + $request->add_masks;
        $nouveau_nombre_de_masquev2 = $nouveau_nombre_de_masque - $request->sup_masks;

        $mon_tres_jolie_commerce->name = $request->name;
        $mon_tres_jolie_commerce->code = $request->code;
        $mon_tres_jolie_commerce->adresse = $request->adresse;
        $mon_tres_jolie_commerce->ville = $request->ville;

        $mon_tres_jolie_commerce->nb_masks = $nouveau_nombre_de_masquev2;
        $mon_tres_jolie_commerce->max_masks = $request->max_masks;

        $mon_tres_jolie_commerce->save();

        return back()->with("success", "Bien joué frère");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
