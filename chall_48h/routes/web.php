<?php 

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'WelcomeController@index')->name('welcome');

Auth::routes();

Route::group(['middleware' => ['auth']], function() {
    Route::resource('home','HomeController')->except(['create']);
    Route::resource('crud_user','Crud_userController')->except(['create'])->middleware(['permission:user|admin']);
    Route::resource('crud_pro','Crud_proController')->except(['create'])->middleware(['permission:admin']);
});