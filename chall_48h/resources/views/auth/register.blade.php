@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card border-0 shadow rounded">
                <div class="card-header border-0">{{ __('Inscription') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nom Complet') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="rounded-pill form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Adresse Mail') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="rounded-pill form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Mot de passe') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="rounded-pill form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirmation') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="rounded-pill form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>

                        </div>


                        <div class="form-group row">
                            <label for="adresse" class="col-md-4 col-form-label text-md-right">Adresse:</label>

                            <div class="col-md-6">
							<input type="text" class="rounded-pill form-control" id="adresse" name="adresse">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="ville" class="col-md-4 col-form-label text-md-right">Ville:</label>

                            <div class="col-md-6">
							<input type="text" class="rounded-pill form-control" id="ville" name="ville">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="code" class="col-md-4 col-form-label text-md-right">Code postale</label>

                            <div class="col-md-6">

                            <input type="text" class="rounded-pill form-control" id="code" name="code">
                            </div>
						</div>

						<div class="form-group row">
                            <div class="col-4 text-right">
                            <input type="radio" id="pro" name="check" value="pro">
                            </div>

                            <div class="col-md-6">                            
                                <label for="pro">Je suis un professionnel</label>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-4 text-right">

                            <input type="radio" id="user" name="check" value="user" checked>
                            </div>
                            <div class="col-md-6">
                                <label for="user">Je suis un client</label>
                            </div>
						</div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="rounded-pill btn btn-primary rounded-pill">
                                    {{ __("S'inscrire") }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
